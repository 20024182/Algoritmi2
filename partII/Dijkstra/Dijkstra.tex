%&latex

\chapter{Algoritmo di Djikstra}
Come per Bellman-Ford (si veda più avanti), questo algoritmo risolve il problema di trovare i cammini minimi su grafo pesato con \textbf{funzione peso definita a valori positivi}. Risulta essere un algoritmo migliore di Bellman-Ford, in termini di complessità computazionale. \\
L'impossibilità di trattare grafi con peso negativo (si noti che non si tratta di \textbf{cicli negativi}, per cui il problema sarebbe non ben posto) è dovuta alla scelta greedy. Essa è localmente ottima, ma non globalmente. Questo fatto fa si che in un determinato istante di tempo \(t\), l'algoritmo è convito che la scelta da lui fatta e mai ripensata sia globalmente ottima, ma non vedendo oltre la frontiera non ne ha alcuna certezza. In sostanza gli mancano delle informazioni, al fine di risolvere il problema più generale. \\
Si noti infine che la struttura rimane sempre quella di una visita: infatti, non a caso, questo algoritmo gode delle proprietà di una visita.

\begin{theorem}[Teorema di sottostruttura ottima dei cammini minimi]
Sia \(G=\left ( V,E \right )\) un grafo pesato con funzione peso \(w:E\rightarrow \mathbb{R}\), sia \(p\) un qualunque cammino minimo in G t.c. \(p=\left \langle v_{x},\ldots,v_{y} \right \rangle\), siano \(v_i\) e \(v_j\) due nodi per cui il cammino \(v_i \leadsto v_j \in p\), ovvero \(p=\left \langle v_{x},\ldots,v_{i},\ldots,v_{j},\ldots,v_{y} \right \rangle\).

Allora il sottocammino \(v_i \leadsto v_j\) è minimo.
\end{theorem}
\begin{proof}
Sarà sviluppata per assurdo.

(Nego la tesi) Il sottocammino \(v_i \leadsto v_j\) \textbf{NON} è minimo, allora deve necessariamente esistere un sottocammino \({p}'\) migliore e distinto da \(v_i \leadsto v_j\). Se viene sostituito \(v_i \leadsto v_j\) in \(p\) con \({p}'\), allora il cammino risultante sarà ``più corto": assurdo, in contrasto con l'ipotesi!
\end{proof}

Segue lo pseudo-codice dell'algoritmo:
\begin{algorithm}[H]
\caption{Algoritmo di Djikstra}
\begin{algorithmic}[1]
\Procedure{Djikstra}{$G,s$} \Comment{$G=\left ( V,E \right )$: grafo pesato con pesi a valori reali, $s$: nodo sorgente}
\State $S \gets\left \{ s \right \}$
\State $A\gets \emptyset$
\State d[$s$] $\gets 0$
\While{possibile}
\State scegli $\left ( u,v \right ) \in E$ t.c. $u \in S \wedge v \in V \setminus S$ e minimizza d[$v$] $\gets$ d[$u$]+peso($u,v$)\label{lst:line:choiceDijkstra}
\State \hspace{\algorithmicindent} $S \gets S \cup \left \{ v \right \}$
\State \hspace{\algorithmicindent} $A \gets A \cup \left \{ \left ( u,v \right ) \right \}$
\State \hspace{\algorithmicindent} d[$v$] $\gets$ d[$u$]+peso($u,v$)
\EndWhile
\EndProcedure
\end{algorithmic}
\end{algorithm}

\begin{theorem}[Correttezza di \textproc{Djikstra}]
Sia \(G=\left ( V,E \right )\) un grafo, connesso e pesato con funzione peso \(w: E \rightarrow \mathbb{R}^{+}\), sia $s \in V$ il nodo sorgente.

Allora l'algoritmo \textproc{Djikstra} calcola correttamente i cammini minimi dalla singola sorgente $s$.
\end{theorem}
\begin{proof}
Sarà svolta per induzione.

Il \textbf{parametro} $k$ che cresce è la cardinalità di $S$. D'ora in avanti verrà usata la seguente notazione per indicare la distanza minima dalla sorgente a un generico nodo, \(\delta \left ( s,v \right )\). Inoltre qualora l'algoritmo calcoli correttamente la minima distanza dalla sorgente deve necessariamente valere che d[$v$]$=\delta \left ( s,v \right )$.
\begin{labeling}{Longer label\quad}
\item [Base] Per \(\left | S \right |=1\) è banalmente verificato: infatti si ha solo la sorgente, la distanza di \(s\) da se stesso è proprio \(0\). In simboli d[$s$]$=\delta \left ( s,s \right )=0$;
\item [Passo induttivo] Se è vero che d[$u$]$=\delta \left ( s,u \right ) \forall u\in S$ per un qualche cammino minimo da \(s\) a \(u\) quando $\left | S \right |=k$ nodi. \\
Allora per $\left | S \right |=k+1$ nodi ovvero: aggiungendo il nodo $v$ dell'arco $\left ( u,v \right )$ che attraversa la frontiera si ha d[$v$]$=\delta \left ( s,v \right )$.
\end{labeling}
La dimostrazione sarà fatta per minorazioni successive. \\
Si supponga di avere un cammino $c$ \textbf{qualunque} distinto da quello greedy  per raggiungere $v$ da sorgente; si mostrerà che esso non è migliore di quello calcolato dall'algoritmo.

Tale cammino è formato da 3 parti: la lunghezza fino a un certo nodo $w$ già scoperto, il peso dell'arco per attraversare la frontiera e infine la lunghezza del sottocammino fino alla destinazione $v$. Tuttavia, sarebbe comodo confrontare il cammino greedy e $c$ fino all'attraversamento della frontiera. Per tanto si ignori il sottocammino $t\leadsto v$ formato solo da nodi \textbf{non scoperti} oppure vuoto: infatti l'arco che attraversa la frontiera potrebbe aver già raggiunto la destinazione. Si noti che se esso fosse negativo questo comporterebbe un aumento della lunghezza di $c$.
\begin{align}
\text{lungh($c$)} &=\text{lungh($s\leadsto w$)}+\text{peso($w,t$)}+\text{lungh($t\leadsto v$)} \geq \\
&\geq \text{lungh($s\leadsto w$)}+\text{peso($w,t$)} \geq \label{removeLastPath} \\
&\geq \text{$\delta (s,w)$}+\text{peso($w,t$)} = \label{usoIpoIndu1} \\
&= \text{d[$w$]}+\text{peso($w,t$)} \geq \label{usoIpoIndu2} \\
&\geq  \text{d[$u$]}+\text{peso($u,v$)}  \label{usoIpoAlgo}
\end{align}
Il passaggio da \ref{removeLastPath} a \ref{usoIpoIndu1} è giustificato vista l'assunzione fatta: infatti \(c\) è un cammino qualunque, quindi non è detto che al suo interno ci sia un sottocammino minimo. Tuttavia, a noi serve comparare cammini minimi quindi facciamo la minorazione.  \\
Si noti inoltre, l'uso delle ipotesi. Dal passo \ref{usoIpoIndu1} al passo \ref{usoIpoIndu2} è legittimo per ipotesi induttiva. Il passo \ref{usoIpoAlgo} è legittimo per ipotesi dell'algoritmo a linea \ref{lst:line:choiceDijkstra}: infatti la scelta che fa l'algoritmo tra tutti i possibili archi è quella migliore nell'istante considerato. Questo significa che la sua scelta non poteva essere diversamente, a meno che non ve ne fosse una equivalente.
\end{proof}

Per valutarne la complessità bisogna considerare un'implementazione meno astratta e più aderente alla realtà.
\begin{algorithm}[H]
\caption{Algoritmo di Djikstra}
\begin{algorithmic}[1]
\Procedure{Djikstra}{$G,s$} \Comment{$G=\left ( V,E \right )$: grafo pesato con pesi a valori reali, $s$: nodo sorgente}
\State $S \gets\left \{ s \right \}$
\State $A\gets \emptyset$
\State d[$s$] $\gets 0$
\State MinHeap heap
\ForAll{$\left ( s,u \right )$} \Comment{arco uscente (o incidente)}
\State $\text{heap}\gets \left ( \left ( s,u \right ), \text{peso}\left ( s,u \right ) \right )$
\EndFor 
\While{$\text{heap} \neq \emptyset$}
\State $\left ( u,v \right )\gets \text{heap}$
\If{$v \not \in S$} \Comment{non fare niente se $v$ è già visitato}
\State $S\gets S\cup \left \{ v \right \}$
\State $A \gets A \cup \left \{ \left ( u,v \right ) \right \}$
\State d[$v$] $\gets$ d[$u$] $+$ peso$\left ( u,v \right )$
\ForAll{$\left ( u,w \right )$} \Comment{arco uscente (o incidente)}
\If{$w \not \in S$} \Comment{piccola ottimizzazione, non impatta su $O$}
\State $\text{heap}\gets\left ( \left ( v,w \right ),\text{d[}v \text{]}+\text{peso}\left ( v,w \right ) \right )$
\EndIf
\EndFor
\EndIf
\EndWhile
\EndProcedure
\end{algorithmic}
\end{algorithm}

%TODO