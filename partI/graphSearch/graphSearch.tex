%&latex

\chapter{Visite di grafi}
In questo capitolo affronteremo lo studio degli algoritmi di visita di un grafo. Per ognuno di essi, oltre a dimostrarne la correttezza, cercheremo di coglierne le \textbf{proprietà}. Queste ultime saranno utili per affrontare il capitolo successivo, nel quale introdurremo alcune applicazione delle visite.

\section{Visita generica di un grafo}
Di seguito è proposta una versione generica di algoritmo per la visita di un grafo qualunque:

\begin{algorithm}[H]
\caption{Visita generica di un grafo}
\begin{algorithmic}[1]
\Procedure{Generic-Graph-Search}{$G,s$} \Comment{$G=\left ( V,E \right )$: grafo, $s \in V$: nodo sorgente}
\State $S \gets \left \{ s \right \}$
\State $A\gets \emptyset$
\While{possibile}
\State scegli $\left ( u,v \right ) \in E$ t.c. $u \in S \wedge v \in V \setminus S$ \label{lst:line:choiceGGS}
\State \hspace{\algorithmicindent} $S \gets S \cup \left \{ v \right \}$
\State \hspace{\algorithmicindent} $A \gets A \cup \left \{ \left ( u,v \right ) \right \}$
\EndWhile
\EndProcedure
\end{algorithmic}
\end{algorithm}

Non sarà discussa la complessità di tale pseudo-codice. Quest'ultimo risulta troppo ad alto livello, per essere facilmente trattato in termini di complessità. Risulta così distante da una possibile implementazione reale, che alla linea \ref{lst:line:choiceGGS} non è nemmeno indicata come viene fatta la scelta.

\begin{theorem}[Correttezza di \textproc{Generic-Graph-Search}]
Sia \(G=\left ( V,E \right )\) un grafo, sia \(s \in V\) un nodo sorgente, siano \(S\) ed \(A\) rispettivamente l'insieme dei nodi e degli archi scoperti a partire da \(s\).

Allora la procedura \textproc{Generic-Graph-Search} scopre tutti i nodi raggiungibili dalla sorgente \(s\).
\end{theorem}
\begin{proof}
Sapendo che un nodo qualunque \(v\) è raggiungibile dalla sorgente \(s\), se esiste un cammino, allora
la dimostrazione sarà per induzione e come \textbf{parametro} \(k\) si sceglie la lunghezza del cammino.

\begin{labeling}{Longer label\quad}
\item [Base] Per \(k=0\) è banalmente verificato: infatti si ha solo la sorgente, la distanza di \(s\) da se stessa è proprio \(0\). Inizializzazione dell'algoritmo;
\item [Passo induttivo] Se è vero per un qualche cammino da \(s\) a \(u\) di lunghezza \(k\). \\
Allora sarà vero anche per il cammino da \(s\) a \(v\), attraversando l'arco \(\left ( u,v \right )\), con lunghezza \(k+1\).
\end{labeling}
Vista la scelta operata dall'algoritmo (si veda linea \ref{lst:line:choiceGGS}) è evidente l'esaustività della visita: infatti il nodo \(v\) non viene mai visto più di una volta. Vista l'assunzione fatta per la lunghezza del cammino \(k\), l'aggiunta dell'arco \(\left ( u,v \right )\) cade su un nodo, per l'appunto \(v\), che sicuramente non era ancora visitato. Per tanto la scelta ripetuta, ma in ogni istante su insiemi \(S\) e \(V \setminus S\) diversi, comporterà il raggiungimento, se possibile di tutti i nodi raggiungibili da \(s\). Si è derivata la tesi.
\end{proof}

Le visite godono di alcune proprietà: terminano, scoprono tutti i nodi raggiungibili dalla sorgente e costruisco un albero. Di seguito sarà dimostrata quest'ultima proprietà per il \textbf{solo} caso di grafo non orientato.
\begin{lemma}
Sia \(G=\left ( V,E \right )\) un grafo non orientato, sia \(s \in V\) un nodo sorgente, siano \(S\) ed \(A\) rispettivamente l'insieme dei nodi e degli archi scoperti a partire da \(s\).

Allora la procedura \textproc{Generic-Graph-Search} costruisce un albero \({G}'=\left ( S,A \right )\) radicato in \(s\) t.c. \(S \subseteq V\) e \(A \subseteq E\).
\end{lemma}
\begin{proof}
Sarà sviluppata per induzione.

Il \textbf{parametro} \(k\) che cresce è la cardinalità di \(A\).

\begin{labeling}{Longer label\quad}
\item [Base] Per \(\left | A \right |=0\) è banalmente verificato: infatti si ha solo la sorgente, \(s\in S\), per cui l'albero \(G'\) è costituito da un solo nodo: ovvero la radice;
\item [Passo induttivo] Se è vero che \({G}'\) è un albero radicato in \(s\) con \(\left | A \right |=k\) archi.

Allora l'aggiunta dell'arco \(\left ( u,v \right )\), che attraversa la frontiera e porta al nodo \(v\), fa si che \({G}'\) con \(\left | A \right |=k+1\) archi rimanga un albero radicato in \(s\).
\end{labeling}
Per dimostrare la tesi induttiva deve valere che: l'aggiunta dell'arco \(\left ( u,v \right )\) non fa perdere le caratteristiche di albero ad \(A\). Esso risulta essere ancora un albero, infatti è:
\begin{itemize}
\item \textit{non orientato}: per ipotesi del lemma;
\item \textit{connesso}: perché all'aggiunta del nodo vi è corrispondente l'aggiunta di un arco che ne permette la raggiungibilità;
\item \textit{aciclico}: perché raggiungo un vertice non ancora scoperto, perciò ho la garanzia di non formare cicli. \qedhere
\end{itemize}
\end{proof}
In \textproc{Generic-Graph-Search}, come abbiamo precedentemente già detto, si è volutamente omesso la spiegazione di come l'arco viene scelto. Per tanto a seguire saranno presentate due possibili visite. Per ciascuna sarà presentato lo pseudo-codice, la complessità e le proprietà.

\input{partI/graphSearch/contents/BFS.tex}
\input{partI/graphSearch/contents/DFS.tex}

